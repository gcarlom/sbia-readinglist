## Reading List ##

from Spring Boot in Action by Craig Walls

Create the skeleton for the project with Sprint Inizializr https://start.spring.io
select:
* Maven
* Java 8 Project

 and dependencies:

* Web
* Thymeleaf
* JPA
* H2

Import the project in Intellij
* Copy the generated zip (readinglist.zip) into : workspace\spring-boot-in-action-demo
* Unzip it there.The root dir of the unzipped file will be  workspace\spring-boot-in-action-demo\readinglist
* In Intellij (Comon Edition) >  Import Project
 select the dir *above* the readinglist root dir, that is select  workspace\spring-boot-in-action-demo
 click on 'Next' (3? times) 
 you will get at the end:
  project files location= C:\Users\gc\workspace\spring-boot-in-action-demo\readinglist
* Import project from external model
  and select in dialog:
  "Root directory" = workspace\spring-boot-in-action-demo\readinglist


To build
 mvn clean install
  (in workspace\spring-boot-in-action-demo\readinglist. Jar is created under *target* dir)

To run the application (in workspace\spring-boot-in-action-demo\readinglist) within the embedded server:

* java -jar target/readinglist-0.0.1-SNAPSHOT.jar

 or
* mvn spring-boot:run
